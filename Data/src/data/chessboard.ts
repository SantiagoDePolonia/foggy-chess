import {ChessboardType, ChessPiece, ChessColor} from 'foggy-chess-logic/build/definitions/chess'

const startingChessboard: ChessboardType = {
    "turn": ChessColor.white,
    "rows": [
        [
            {
                "color": "white",
                "piece": ChessPiece.rook
            },
            {
                "color": "white",
                "piece": ChessPiece.knight
            },
            {
                "color": "white",
                "piece": ChessPiece.bishop
            },
            {
                "color": "white",
                "piece": ChessPiece.queen
            },
            {
                "color": "white",
                "piece": ChessPiece.king
            },
            {
                "color": "white",
                "piece": ChessPiece.bishop
            },
            {
                "color": "white",
                "piece": ChessPiece.knight
            },
            {
                "color": "white",
                "piece": ChessPiece.rook
            }
        ],
        [
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            },
            {
                "color": "white",
                "piece": ChessPiece.pawn
            }
        ],
        [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ],
        [
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            },
            {
                "color": "black",
                "piece": ChessPiece.pawn
            }
        ],
        [
            {
                "color": "black",
                "piece": ChessPiece.rook
            },
            {
                "color": "black",
                "piece": ChessPiece.knight
            },
            {
                "color": "black",
                "piece": ChessPiece.bishop
            },
            {
                "color": "black",
                "piece": ChessPiece.queen
            },
            {
                "color": "black",
                "piece": ChessPiece.king
            },
            {
                "color": "black",
                "piece": ChessPiece.bishop
            },
            {
                "color": "black",
                "piece": ChessPiece.knight
            },
            {
                "color": "black",
                "piece": ChessPiece.rook
            }
        ]
    ]
};

export default startingChessboard;
import {connectToDatabase} from '../utils/mangodb';

async function getChessboardByGameId(gameId) {
    const { db } = await connectToDatabase()

    const collection = db.collection('myCollection');
    const chessboard = await collection.findOne({ gameId });

    return chessboard;
}

export default getChessboardByGameId;
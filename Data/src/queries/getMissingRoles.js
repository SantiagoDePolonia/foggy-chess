import {connectToDatabase} from '../utils/mangodb';

async function getMissingRoles(gameId) {
    const { db } = await connectToDatabase()

    const sides = ["white", "black"];

    const collection = db.collection('myCollection');
    const chessboard = await collection.findOne({ gameId });

    if(chessboard.players) {
      const sidesAssigned = Object.keys(chessboard.players);
      return sides.filter(side => !sidesAssigned.includes(side));
    }

    return sides;
}

export default getMissingRoles;

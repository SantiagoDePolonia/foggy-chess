import getChessboardByGameId from './getChessboardByGameId';

async function getPlayerRoleInGame(gameId, playerId) {
    const game = await getChessboardByGameId(gameId);

    if(game.players) {
        for(const side in game.players) {
            if(game.players[side] === playerId) {
                return side;
            }
        }
    }
    return false;
}

export default getPlayerRoleInGame;

import {connectToDatabase} from '../utils/mangodb';

import generateGameId from '../utils/generateGameId';

const getUniqGameId = async () => {
  let results;
  let gameId;

  const { db } = await connectToDatabase()
  const collection = db.collection('myCollection');

  do {
      gameId = generateGameId();
      results = collection.findOne({
          gameId
      });
  }

  while (!results);

  return gameId;
}

export default getUniqGameId;

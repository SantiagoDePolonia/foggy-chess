import {connectToDatabase} from '../utils/mangodb';

import startingChessboard from '../data/chessboard';

import getUniqGameId from './getUniqGameId';
import setPlayerRoleInGame from './setPlayerRoleInGame';

async function initGame(playerId = null) {
  const assignRole = playerId ? true : false
  const gameId = await getUniqGameId();

  const { db } = await connectToDatabase();
  // @todo - db connection checking
  // const isConnected = await client.isConnected();

  const collection = db.collection('myCollection');
  
  collection.insertOne({
      gameId,
      chessboardState: startingChessboard,
  });

  if(assignRole) {
    await setPlayerRoleInGame(gameId, playerId);
  }

  return gameId;
}

export default initGame;

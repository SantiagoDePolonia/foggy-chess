import {connectToDatabase} from '../utils/mangodb';

async function setChessboardStateByGameId(gameId, chessboardState) {
    const { db } = await connectToDatabase()

    const collection = db.collection('myCollection');
    const chessboard = await collection.updateOne({ gameId }, {
      "$set": {
        chessboardState
      }
    });

    return chessboard;
}

export default setChessboardStateByGameId;
import {connectToDatabase} from '../utils/mangodb';

import getPlayerRoleInGame from './getPlayerRoleInGame';
import getMissingRoles from './getMissingRoles';

/**
 * The function checks whether role exist in some game.
 * If not - the function assign missing role or return false if all roles are reselved
 * @param {*} gameId 
 * @param {*} playerId 
 * @param {string} order - white | black | random
 * 
 * @returns white | black | other
 */
async function setPlayerRoleInGame(gameId, playerId, order = "white") {
  const role = await getPlayerRoleInGame(gameId, playerId);

  if(role) {
    return role;
  }

  if(order !== "white" && order !== "black") {
    order = Math.floor(Math.random()*2) % 2 ? "white" : "black";
  }

  const missingRoles = await getMissingRoles(gameId);

  if(missingRoles.length) {
    const { db } = await connectToDatabase()
    const collection = db.collection('myCollection');
    let role;

    if(missingRoles === 2 ) {
      role = order;
    } else {
      role = missingRoles[0];
    }
    const updatedRole = `players.${role}`;
    await collection.updateOne({ gameId }, {
      "$set": {
        [updatedRole]: playerId
      }
    });
    return role;
  }

  return "other";
}

export default setPlayerRoleInGame;

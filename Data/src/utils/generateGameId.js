
const MAX = 122;
const MIN = 48;

const IGNORED_ASCII_DEC = [
  // 48-57 | 0-9
  58, 59, 60, 61, 62, 63, 64,
  // 65-90 | a-z
  91, 92, 93, 94, 95, 96
  // 97-122 | A-Z
];
 
// generate ASCII character string (dec between 44-126)
export default function generateGameId( length = 6, string = "" ) {
    if(length) {
      let number = 0;
      
      do {
        number = Math.floor(
          Math.random() * (MAX-MIN + 1)
        ) + MIN
      } while(IGNORED_ASCII_DEC.indexOf(number) != -1);

        string += generateGameId(length-1, 
            String.fromCharCode(number)
        );
    }

    return string;
}

import cookie from 'cookie';
import { v4 as uuidv4 } from 'uuid';

export function getPlayerId(req, res) {
    var cookies = cookie.parse(req.headers.cookie || '');
    const playerId = cookies.playerId;
    if(playerId) {
        return playerId;
    }

    return setPlayerId(res);
}

// modify response and setCookie with playerId name, expires after 10 years
export function setPlayerId(res): string {
    const uuid = uuidv4()
    const seconds10Years = 60 * 60 * 24 * 365 * 10;
    const now = new Date();
    const expires = new Date(now.getTime() + 1000 * seconds10Years); // ~10 years
    res.setHeader('Set-Cookie', cookie.serialize('playerId', uuid, {
        expires
    }));

    return uuid;
}

import { ChessboardRows,
  chessColorType
} from "../definitions/chess";
import ChessboardDataInterface from './ChessboardDataInterface';
import ChessboardData from './ChessboardData';

abstract class Chessboard {
  chessboardData: ChessboardDataInterface;

  constructor(chessboardData: ChessboardDataInterface) {
    this.chessboardData = new ChessboardData(chessboardData);
  }

  get turn() {
    return this.chessboardData.turn;
  }

  set turn(value: chessColorType) {
    this.chessboardData.turn = value;
  }

  get check() {
    return this.chessboardData.check;
  }

  set check(value: boolean) {
    this.chessboardData.check = value;
  }

  get rows() {
    return this.chessboardData.rows;
  }

  set rows(value: ChessboardRows) {
    this.chessboardData.rows = value;
  }
}

export default Chessboard;

import Chessboard from './Chessboard';
import ChessboardPerspectived from './ChessboardPerspectived';

import { ChessboardType } from '../definitions/chess';
import Position from './Position';
import ChessboardManager from './ChessboardManager';

class ChessboardArbitrator extends Chessboard {

  chessboards = {
    white: null,
    black: null
  }

  constructor(chessboardData) {
    super(chessboardData);
    this.chessboardData;
    this.chessboards.white = new ChessboardPerspectived(this.chessboardData, "white");
    this.chessboards.black = new ChessboardPerspectived(this.chessboardData, "black");
  }

  movePiece(pFrom: Position, pTo: Position) {
    const chessboardManager = new ChessboardManager(this.chessboardData);
    const moveResult = chessboardManager.movePiece(pFrom, pTo);
    if(moveResult) {
      this.chessboards.white.update();
      this.chessboards.black.update();
      return true;
    }
    return false;
  }

  getChessboard(perspective) : ChessboardType {
    const rows = [...this.rows];

    for(let row = 0; row < 8; row++) {
      rows[row] = [...this.rows[row]];
      for(let col = 0; col < 8; col++) {
        if( this.chessboards[perspective].chessboard[row][col].foggy &&
          this.rows[row][col] &&
          this.rows[row][col].color !== perspective
        ) {
          rows[row][col] = null;
        }
      }
    }

    return {
      rows,
      perspective,
      turn: this.turn
    }
  }
}

export default ChessboardArbitrator;
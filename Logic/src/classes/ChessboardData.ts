import { ChessboardRows, chessColorType } from "../definitions/chess";
import ChessboardDataInterface from './ChessboardDataInterface';

class ChessboardData implements ChessboardDataInterface {
  rows: ChessboardRows;
  check: boolean;
  turn: chessColorType;

  constructor({ rows, turn, check }) {
    this.rows = rows;
    this.turn = turn;
    this.check = check;
  }
}

export default ChessboardData;

import { ChessboardRows, chessColorType } from "../definitions/chess";

interface ChessboardDataInterface {
  rows: ChessboardRows;
  check: boolean;
  turn: chessColorType;
}

export default ChessboardDataInterface;
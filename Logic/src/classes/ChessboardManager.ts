import ChessboardData from "./ChessboardData";
import Position from "./Position";

class ChessboardManager {
  chessboardData: ChessboardData;

  constructor(chessboardData: ChessboardData) {
    this.chessboardData = chessboardData;
  }

  movePiece(from: Position,  to: Position) {

    if(!this.chessboardData.rows[from.row][from.col]) {
      return false;
    }
    // when the moved piece is not in turn, return false
    else if(this.chessboardData.turn !== this.chessboardData.rows[from.row][from.col].color) {
      return false;
    }

    // TODO: 1. Validate move
    // TODO: 2. Prevent king killking. What to do when the chessboard is in check state?
    const piece = this.chessboardData.rows[from.row][from.col];

    this.chessboardData.rows[to.row][to.col] = {...piece};
    this.chessboardData.rows[from.row][from.col] = null;

    this.toggleTurn();

    return true;
  }

  private toggleTurn(): void {
    if(this.chessboardData.turn === "white") {
      this.chessboardData.turn = "black";
    } else {
      this.chessboardData.turn = "white";
    }
  }
}

export default ChessboardManager;
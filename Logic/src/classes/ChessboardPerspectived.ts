import {
  ChessPiece,
  chessboardPerspective,
  ChessboardRowType,
  ChessPieceType,
  ChRow,
  ChCol,
  ChessboardFoggyRows,
} from "../definitions/chess";

import Position from './Position';
import ChessboardDataInterface from './ChessboardDataInterface';

import Chessboard from './Chessboard';

class ChessboardPerspectived extends Chessboard {
  perspective: chessboardPerspective;
  chessboardFoggyRows: ChessboardFoggyRows;
  private rowVector: number;
  private lastRow: ChRow;

  // TODO: ChessboardType -> ChessboardPerspectivedType
  constructor(chessboardData: ChessboardDataInterface, perspective) {
    super(chessboardData);

    this.perspective = perspective;

    this.rowVector = this.perspective === chessboardPerspective.white ? 1 : - 1;
    this.lastRow = this.perspective === chessboardPerspective.white ? 7 : 0;

    this.initFoggyChessboard();
    this.update();
  }

  update() {
    this.resetFoggyChessboard();
    this.unfoggyChessboard();
  }

  private initFoggyChessboard() {
    this.chessboardFoggyRows = [];
    for(let row: number = 0; row<8; row++) {
      this.chessboardFoggyRows[row] = [];
    }
  }

  private resetFoggyChessboard() {
    for(let row: number = 0; row<8; row++) {
      for(let col: number = 0; col<8; col++) {
        this.chessboardFoggyRows[row][col] = true;
      }
    }
  }

  get chessboard() {
    const chessboard = [];
    for(let row: number = 0; row<8; row++) {
      chessboard[row] = []

      for(let col: number = 0; col<8; col++) {
        chessboard[row][col] = {
          piece: this.rows[row][col],
          foggy: this.chessboardFoggyRows[row][col]
        }
      }
    }

    return chessboard;
  }

  private unfoggyChessboard() : void {
    const unfoggedFields: Position[] = [];

    this.rows.forEach((row: ChessboardRowType, iRow: ChRow) => {
      row.forEach((piece: ChessPieceType | null, iCol: ChCol) => {
        if(piece) {
          if(this.perspective === piece.color) {
            const position = new Position(iCol, iRow);

            unfoggedFields.push(
              position,
              ...this._adjacentFields(position),
              ...this.getPossibleMoves(piece.piece, position)
            );
          }
        }
      });
    });

    unfoggedFields.forEach(({row, col}: Position) => {
      this.chessboardFoggyRows[row][col] = false;
    }, this);
  }

  private getPossibleMoves(
    piece: ChessPiece,
    position : Position
  ) : Position[] {
    const {row, col} = position;
    const possibleFields: Position[] = [];

    switch(piece) {
      case ChessPiece.pawn: {
        if( row === this.lastRow ) {
          // TODO: promote pawn ??
          break;
        }

        const rowForward = row + (1*this.rowVector);

        if( !this.rows[rowForward][col]) {
          possibleFields.push(new Position(
            col,
            rowForward
          ));
          
          const rowDoubleForward = row + (2*this.rowVector)
          if( 
            this.lastRow - 6*this.rowVector === row &&
            !this.rows[rowDoubleForward][col]
          ) {
            possibleFields.push(new Position(
              col,
              rowDoubleForward
            ));
          }
        }

        const attackedFields = [
            new Position(
                col+1,
                row+this.rowVector
            ),
            new Position(
                col-1,
                row+this.rowVector
            )
        ];

        attackedFields.forEach((position) => {
            if(!position.isValid()) {
                return;
            }

            if(this._isPositionOccupiedByAttackablePiece(position)) {
                possibleFields.push(position);
            }    
        });

        break;
      }

      case ChessPiece.king: {
        for(let i = row-1; i <= row+1; i++) {
          if(i > 7 || i < 0)
            continue;
          
          for(let j = col-1; j <= col+1; j++) {
            if(j > ChCol.H || j < ChCol.A)
              continue;
            
            if(this.rows[i][j])
              continue;

            // @TODO: checking: Is the field under attack?

            possibleFields.push(new Position(j, i))
          }
        }
        break;
      }
      case ChessPiece.knight: {
        const vectors = [
          [-2,-1],
          [-2, 1],
          [2, -1],
          [2, 1],
          [-1, -2],
          [-1, 2],
          [1, -2],
          [1, 2]
        ];

        vectors.forEach(([vCol, vRow]) => {
            const position: Position = new Position(col+vCol, row+vRow);

            if(!position.isValid()) {
              return;
            }

            if(!this._isPositionEmpty(position) && !this._isPositionOccupiedByAttackablePiece(position)) {
              return;
            }

            possibleFields.push(new Position(position.col, position.row));
        });
        break;
      }
      case ChessPiece.bishop: {
        const vectors = [
          [1, 1],
          [1, -1],
          [-1, 1],
          [-1, -1],
        ];

        vectors.forEach((vector) => {
          possibleFields.push(
            ...this._findPath(position, vector)
          );
        });

        break;
      }
      case ChessPiece.rook: {
        const vectors = [
          [0, 1],
          [0, -1],
          [-1, 0],
          [1, 0],
        ];

        vectors.forEach((vector) => {
          possibleFields.push(
            ...this._findPath(position, vector)
          );
        });

        break;
      }

      case ChessPiece.queen: {
        const vectors = [
          // bishop vectors + rook vectors
          [1, 1],
          [1, -1],
          [-1, 1],
          [-1, -1],
          [0, 1],
          [0, -1],
          [-1, 0],
          [1, 0],
        ];

        vectors.forEach((vector) => {
          possibleFields.push(
            ...this._findPath(position, vector)
          );
        });

        break;
      }
    }
    return possibleFields;
  }

  _isPositionEmpty(position: Position): boolean {
    if(this.rows[position.row][position.col]) {
      return false;
    }

    return true;
  }

  _isPositionOccupiedByAttackablePiece(position: Position): boolean {
    const piece = this.rows[position.row][position.col];
    if(!piece) {
      return false;
    }

    if(piece.color !== this.perspective && piece.piece !== ChessPiece.king) {
      return true;
    }

    return false;
  }

  _findPath(
    source: Position,
    vector: number[],
    recursive: boolean = true,
    path: Position[] = []
  ): Position[] {
    const position: Position = new Position(source.col + vector[1], source.row + vector[0]);

    if(position.isValid()) {
      if(this._isPositionEmpty(position)) {
        path.push(position);
        if(recursive) {
          this._findPath(position, vector, recursive, path);
        }
      } else if(this._isPositionOccupiedByAttackablePiece(position)) {
        path.push(position);
      }
    }

    return path;
  }

  _adjacentFields(position: Position) : Position[] {
    const vectors = [
      [1, 1],
      [1, -1],
      [-1, 1],
      [-1, -1],
      [0, 1],
      [0, -1],
      [-1, 0],
      [1, 0],
    ];
    const adjacentFields: Position[] = [];

    vectors.forEach(vector => {
      const adjacentPosition = new Position(position.col + vector[1], position.row + vector[0]);

      if(adjacentPosition.isValid()) {
        adjacentFields.push(adjacentPosition);
      }
    });

    return adjacentFields;
  }
}

export default ChessboardPerspectived;

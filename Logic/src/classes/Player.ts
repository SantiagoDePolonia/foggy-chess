class Player {
    id : string;

    constructor(id) {
        this.id = id;
    }

    toString() {
        return this.id;
    }
}

export default Player;

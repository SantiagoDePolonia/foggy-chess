import {ChCol, ChRow} from '../definitions/chess';

class Position {
    public col: ChCol;
    public row: ChRow;

    private colMap: string[] = ["A", "B", "C", "D", "E", "F", "G", "H"];

    constructor(col = undefined, row = undefined) {
        if(typeof col !== "undefined" && typeof row !== "undefined") {
            this.col = col;
            this.row = row;
        }
    }

    setFromName(name: string) : Position {
        this.col = this.colMap.indexOf(name[0]);
        this.row = parseInt(name[1])-1;

        return this;
    }

    isValid() : boolean {
        if(
            this.row < 0 ||
            this.row > 7 ||
            this.col < 0 ||
            this.col > 7
        ) {
            return false;
        }

        return true;

    }

    toString() : string {
        return this.colMap[this.col]+(this.row+1);
    }
};

export default Position;
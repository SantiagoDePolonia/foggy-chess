import Position from '../classes/Position';

export enum ChessColor {
    "white" = "white",
    "black" = "black"
}

export enum ChessPiece {
    "king" = 0,
    "queen" = 1,
    "rook" = 2,
    "bishop" = 3,
    "knight" = 4,
    "pawn" = 5
}

export enum chessboardPerspective {
    white = "white",
    black = "black"
}

export type chessColorType = "white" | "black";


export type chessPiece = ChessPiece.rook | ChessPiece.knight | ChessPiece.bishop | ChessPiece.queen | ChessPiece.king | ChessPiece.pawn;

export type emptyField = {};

export interface ChessboardFieldType {
    piece: ChessPieceType,
    foggy: boolean
}

export interface ChessPieceType {
    color: chessColorType,
    piece: ChessPiece
};

export type ChessboardRowType = Array<ChessPieceType | null>;

export type ChessboardRows = Array<ChessboardRowType>;

export type ChessboardFoggyRows = boolean[][];

// used for transport data over API
export interface ChessboardType {
    perspective: chessboardPerspective,
    turn: chessColorType,
    rows: ChessboardRows
}
export interface ActivePiece {
    piece: ChessPieceType,
    field: string,
    possibleMoves: Position[]
}

// used to generate the components
// it includes data generated on client side, but not transported over API:
// - foggy flag for every field
// - activeField
export type ChessboardStateType = {
    perspective: chessboardPerspective,
    activePiece: ActivePiece,
    rows: ChessboardFieldType[][]
}

export enum ChCol {
    A = 0,
    B = 1,
    C = 2,
    D = 3,
    E = 4,
    F = 5,
    G = 6,
    H = 7
}

export type ChRow = number;

export type FieldPositionType = {
    col: ChCol,
    row: ChRow
};

export interface GameStateWebsocketInterface {
  perspective: chessColorType,
  turn: chessColorType,
  rows: ChessboardRows
}
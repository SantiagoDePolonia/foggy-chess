RULSE
=====

There are two types of fields:
    - foggy - you cannot see what is standing there
    - unfoggy - you can see whether and what stand there

Unfoggy fields:
    - fields on which one of your pions stand on
    - fields on wchich your pion can move in the next move
    - when your field is attacked you can see the field which cause the source of attack

Moving on attacked unfoggy field by king:
    - When you try to move to attacked field, the arbiter (program) will inform you, that this move is not possible.

Others rules are the same as in the traditional chess.
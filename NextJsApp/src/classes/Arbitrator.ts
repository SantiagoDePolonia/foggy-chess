import { io } from "socket.io-client";
import {GameStateWebsocketInterface} from 'foggy-chess-logic/build/definitions/chess';

class Arbitrator {
  socket;
  gameId: string;
  playerId: string;
  chessboard;

  constructor(gameId, playerId, chessboard) {
    this.gameId = gameId;
    this.playerId = playerId;
    this.chessboard = chessboard;

    this.socket = io('localhost:4001/', {query: {
      gameId,
      playerId
    }});

    this.socket.on('gameState', (payload: GameStateWebsocketInterface) => {
      // TODO:
      // payload contain perspective
      // but setChessboardData don't update perspective
      
      this.chessboard.setChessboardData(payload);
    });
  }

  movePiece(from, to) {
    this.socket.emit('move', from.toString(), to.toString());
  }
}

export default Arbitrator;

import { makeObservable, observable, action, computed } from 'mobx';

import ChessboardPerspectived from 'foggy-chess-logic/build/classes/ChessboardPerspectived';
import ChessboardDataInterface from 'foggy-chess-logic/build/classes/ChessboardDataInterface';
import ChessboardData from 'foggy-chess-logic/build/classes/ChessboardData';
import ChessboardManager from 'foggy-chess-logic/build/classes/ChessboardManager';

import Position from 'foggy-chess-logic/build/classes/Position';
import {ChessboardType, ActivePiece, GameStateWebsocketInterface} from 'foggy-chess-logic/build/definitions/chess';

import Arbitrator from './Arbitrator';

class Chessboard extends ChessboardPerspectived {
  arbitrator : Arbitrator;

  activePiece: ActivePiece | null = null;

  constructor({turn, perspective, rows}: GameStateWebsocketInterface) {
    super({turn, rows, check: false}, perspective);

    // I need to be careful with it,
    // because the mobX documentations state that it should be used with Parent class as well
    makeObservable(this, {
        chessboardData: observable,
        perspective: observable,
        chessboardFoggyRows: observable,
        activePiece: observable,
        lastRow: observable,
        update: action,
        movePiece: action,
        selectPiece: action,
        chessboard: computed
    });
  }

  setChessboardData(chessboardData: ChessboardDataInterface) {
    this.chessboardData = new ChessboardData(chessboardData);
    super.update();
  }

  setArbitrator(gameId, playerId) {
    this.arbitrator = new Arbitrator(gameId, playerId, this);
  }

  movePiece(from: Position,  to: Position) {
    const chessboardManager = new ChessboardManager(this.chessboardData);
    
    const isMoved = chessboardManager.movePiece(from, to);
    if(isMoved) {
      this.activePiece = null;
      this.update();

      if(this.arbitrator) {
        // this emit WebSocket message
        this.arbitrator.movePiece(from, to);
      }

      return true;
    }

    return false;
  }

  selectPiece(piece: ChessPieceType, field: string): void {
    if(this.turn !== this.perspective) {
      return;
    }

    if(piece.color !== this.perspective) {
      return;
    }

    this.activePiece = {
      field,
      piece,
      possibleMoves: this.getPossibleMoves(
        piece.piece,
        new Position().setFromName(field)
      )
    }
  }

  getChessboard() : ChessboardType {
    return {
      perspective: this.perspective,
      turn: this.turn,
      rows: this.rows
    }
  }
}

export default Chessboard;

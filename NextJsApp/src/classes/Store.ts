import Chessboard from './Chessboard';
import chessboardData from 'foggy-chess-data/build/data/chessboard';

class Store {
  public chessboardStore;

  constructor(chessboardData) {
    this.chessboardStore = new Chessboard({...chessboardData, perspective: "white"});
  }
}

const storeWrapper = new Store(chessboardData);

export default storeWrapper;

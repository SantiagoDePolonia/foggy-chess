import { makeStyles } from '@material-ui/core';
import classNames from 'classnames';
import React from 'react';
import {ChessPieceType, ChessPiece as ChessPieceEnum} from 'foggy-chess-logic/build/definitions/chess';
import connect from '../../tools/connect';
import get from 'lodash/get';

const useStyles = makeStyles({
    piece: {
        width: "100%",
        height: "100%",
        backgroundImage: "url('/img/pieces/set.svg')",
        backgroundRepeat: "no-repeat",
        backgroundSize: "600% auto",
    },
    white: {
        backgroundPositionY: "0%"
    },
    black: {
        backgroundPositionY: "100%"
    },
    [ChessPieceEnum.rook]: {
        backgroundPositionX: "80%"
    },
    [ChessPieceEnum.knight]: {
        backgroundPositionX: "60%"
    },
    [ChessPieceEnum.bishop]: {
        backgroundPositionX: "40%"
    },
    [ChessPieceEnum.queen]: {
        backgroundPositionX: "20%"
    },
    [ChessPieceEnum.king]: {
        backgroundPositionX: "0%"
    },
    [ChessPieceEnum.pawn]: {
        backgroundPositionX: "100%"
    },
    selected: {
        backgroundColor: "rgba(0, 0, 0, 0.4)",
        border: "1px solid rgba(0, 0, 0, 0.5)"
    },
    myPiece: {
        cursor: "pointer"
    }
});

export interface Props extends ChessPieceType {
    selected: boolean,
    myPiece: boolean,
    selectPiece: () => void,
    name: string
}

function ChessPiece({color, piece, selected, myPiece, selectPiece} : Props) {
    const classes = useStyles();

    return (
        <div data-testid="chess-piece" className={
            classNames(
                classes.piece, 
                classes[color],
                classes[piece],
                {
                    [classes.selected]: selected,
                    [classes.myPiece]: myPiece
                }
            )}
            onClick={selectPiece}
        >

        </div>
    );
}

const storeToProps = (store, props) => ({
    selected: get(store, 'activePiece.field', 'not-existing') === props.name,
    myPiece: store.perspective === props.color,
    selectPiece: (event: MouseEvent) => store.selectPiece({
        piece: props.piece,
        color: props.color
    }, props.name)
});

export default connect(ChessPiece, storeToProps);

import React from 'react';
import classNames from 'classnames';
import get from 'lodash/get';
import { ChessboardFieldType, chessColorType } from 'foggy-chess-logic/build/definitions/chess';

import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import ChessPiece from './ChessPiece';
import connect from '../../tools/connect';
import Position from 'foggy-chess-logic/build/classes/Position';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        flex: 1
    },
    white: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.secondary.light
    },
    black: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.primary.dark
    },
    foggy: {
        filter: "brightness(0.6)",
    },
    actionableField: {
        filter: "brightness(1.6)",
        cursor: "pointer" // fields where I can move
    }
}));

interface Props extends ChessboardFieldType {
    color: chessColorType,
    name: string,
    actionableField: boolean,
    movePiece: () => void
}

function ChessboardField({name, color, foggy, piece, actionableField, movePiece}: Props) {
    const classes = useStyles();

    return (
        <div
            data-testid="chessboard-field"
            title={name}
            className={classNames(
                classes[color],
                classes.root,
                {
                    [classes.foggy]: foggy,
                    [classes.actionableField]: actionableField
                })
            }
            {...(actionableField ? {onClick: movePiece} : {})}
        >
            {piece && 
                <ChessPiece name={name} selected={false} {...piece} />
            }
        </div>
    );
}

const storeToProps = (store, props) => ({
    actionableField: get(store, 'activePiece.possibleMoves', []).some((position) => position.toString() === props.position.toString()),
    piece: store.chessboard[props.position.row][props.position.col].piece,
    foggy: store.chessboard[props.position.row][props.position.col].foggy,
    name: props.position.toString(),
    movePiece: (e : MouseEvent) => store.movePiece(new Position().setFromName(store.activePiece.field), props.position)
});

export default connect(ChessboardField, storeToProps);

import { createStyles, makeStyles } from '@material-ui/core';
import React, { ReactElement } from 'react';
import connect from '../../tools/connect';

import ChessboardField from '../atoms/ChessboardField';

import { chessboardPerspective } from 'foggy-chess-logic/build/definitions/chess';
import Position from 'foggy-chess-logic/build/classes/Position';

const useStyles = makeStyles(() => createStyles({
    root: {
        display: "flex",
        flex: 1
    }
}));

interface Props {
    row: number,
    perspective: chessboardPerspective
};

function ChessboardRow({row, perspective}: Props) {
    
    const classes = useStyles();
    const colsMap = ["A","B","C","D","E","F","G","H"];

    const fieldList: ReactElement[] = colsMap.map((colName, col) => <ChessboardField
        key={col}
        position={new Position(col, row)}
        color={ (row + col)%2 ? "white" : "black" }
    />);

    const colListPerspectived = "white" === perspective ? fieldList : fieldList.reverse();

    return (<div className={classes.root}>
        {colListPerspectived}
    </div>);
}

export default connect(ChessboardRow);

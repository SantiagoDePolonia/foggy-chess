import { makeStyles, createStyles } from "@material-ui/core";
import React from "react";
import ChessboardRow from "../molecules/ChessboardRow";
import {ChessboardFieldType, chessboardPerspective} from 'foggy-chess-logic/build/definitions/chess';
import connect from "../../tools/connect";

const useStyles = makeStyles(() => createStyles({
    root: {
        display: "flex",
        flexDirection:"column",
        margin: "0 auto",
        height: "50vh",
        width: "50vh"
    }
}));

interface Props {
    perspective: chessboardPerspective,
    chessboard: ChessboardFieldType[][],
}

function Chessboard({perspective, chessboard} : Props) {
    const classes = useStyles();

    const rowList = [];

    for(let row: number = 0; row < chessboard.length; row++) {
        rowList.push(<ChessboardRow key={row} row={row} perspective={perspective} />);
    }

    const rowListPerspectived = perspective === "white" ? rowList.reverse() : rowList;

    return (<div data-testid="chessboard" className={classes.root}>
        {rowListPerspectived}
    </div>);
}

const storeToProps = (store) => ({
    perspective: store.perspective,
    chessboard: store.chessboard
});

export default connect(Chessboard, storeToProps);

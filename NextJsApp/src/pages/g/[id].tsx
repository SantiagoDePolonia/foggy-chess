import React from 'react';
import {GetServerSideProps} from 'next';
import { getPlayerId } from 'foggy-chess-data/build/utils/playerId';
import Chessboard from '../../components/organisms/Chessboard';
import ChessboardClass from '../../classes/Chessboard';

import {Provider as StoreProvider} from '../../tools/connect';

import getChessboardByGameId from 'foggy-chess-data/build/queries/getChessboardByGameId';
import setPlayerRoleInGame from 'foggy-chess-data/build/queries/setPlayerRoleInGame';

import store from '../../classes/Store';
export default class GamePage extends React.Component {

    constructor(props: {chessboardState, perspective}) {
        super(props);
        store.chessboardStore = new ChessboardClass({...props.chessboardState, perspective: props.perspective});
    }

    componentDidMount() {
      const { playerId, gameId } = this.props;
      store.chessboardStore.setArbitrator(gameId, playerId);
    }

    // TODO:
    // fire arbitrator
    // // this.socket.disconnect();
    // componentWillUnmount() {
    // }

    render() {
      return (
        <StoreProvider>
          <div data-testid="game-page">
            <h1>Plansza!</h1>
            <Chessboard />
            {/* modal-like Component with pawn promotion select */}
          </div>
        </StoreProvider>
      );    
    }
};

export const getServerSideProps : GetServerSideProps = async ({params: {id: gameId}, req, res}) => {

    const playerId = getPlayerId(req, res);
    const { chessboardState } = await getChessboardByGameId(gameId);
    const perspective = await setPlayerRoleInGame(gameId, playerId);

    return {
        props: {
            chessboardState,
            gameId,
            playerId,
            perspective
        }
    };
}

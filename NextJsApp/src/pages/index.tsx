import {GetServerSideProps} from 'next';
import { getPlayerId } from 'foggy-chess-data/build/utils/playerId';

import Head from 'next/head';

import styles from '../styles/Home.module.css';
import { Button } from '@material-ui/core';


function getGameId(playerId) {
  return fetch('http://localhost:4001/api/game-id', {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        playerId
      })
    })
    .then(response => response.json());
}

function newGame(playerId) {
  getGameId(playerId)
    .then(({id: gameId}) => {
      window.location.href = `/g/${gameId}`
    })
}

export default function Home({playerId}) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Foggy Chess!</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Let's play the Foggy Chess!
        </h1>
        <Button variant="contained" color="primary" size="large" onClick={(e) => (newGame(playerId))}>
          Play!
        </Button>
      </main>
    </div>
  )
}

export const getServerSideProps : GetServerSideProps = async ({params, req, res}) => {
  const playerId = getPlayerId(req, res);

  return {
    props: { playerId }
  };
}

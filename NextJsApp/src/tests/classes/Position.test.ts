import Position from '../../classes/Position';

describe('Position class', () => {
    describe('isValid public method', () => {
        it('returns true when position is valid',() =>{
            const position1 = new Position(0, 0); // A1
            expect(position1.isValid()).toBe(true);

            const position2 = new Position(7, 7); // H8
            expect(position2.isValid()).toBe(true);
        });

        it('returns false when position is invalid', () =>{
            const position1 = new Position(8, 0);
            expect(position1.isValid()).toBe(false);

            const position2 = new Position(0, 8);
            expect(position2.isValid()).toBe(false);

            const position3 = new Position(-1, 0);
            expect(position3.isValid()).toBe(false);

            const position4 = new Position(0, -1);
            expect(position4.isValid()).toBe(false);
        });
    });
});

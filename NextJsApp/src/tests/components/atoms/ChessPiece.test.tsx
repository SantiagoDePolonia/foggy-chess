import { render } from '@testing-library/react';
import React from 'react';
import ChessPiece from '../../../components/atoms/ChessPiece';

describe('<ChessPiece />', () => {
    it('renders', () => {
        const {getByTestId} = render(<ChessPiece color="white" piece="rook" />);
        expect(getByTestId('chess-piece')).toBeTruthy;
    });
});

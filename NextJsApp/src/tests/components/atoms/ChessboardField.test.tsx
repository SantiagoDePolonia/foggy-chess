import React from 'react';
import {render} from '@testing-library/react';
import ChessboardField from '../../../components/atoms/ChessboardField';
import chessboardData from 'foggy-chess-data/build/data/chessboard';

describe('<ChessboardField />', () => {
    it('renders', () => {
        const { getByTestId } = render(<ChessboardField name="A1" color="white" piece={chessboardData.rows[0][0]} />);
        expect(getByTestId('chessboard-field')).toBeTruthy;
    });
});

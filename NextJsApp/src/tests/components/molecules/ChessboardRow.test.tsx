import React from 'react';
import {render} from '@testing-library/react';

import Chessboard from '../../../classes/Chessboard';
import ChessboardRow from '../../../components/molecules/ChessboardRow';
import chessboardData from 'foggy-chess-data/build/data/chessboard';
import {chessboardPerspective} from 'foggy-chess-logic/build/definitions/chess';

describe('<ChessboardRow />', () => {
    const chessboard = new Chessboard(chessboardData);
    it('renders 8 fields', () => {

        const { getAllByTestId } = render(<ChessboardRow row={1} perspective={chessboardPerspective.white} fields={chessboard.chessboard[1]} />);

        expect(getAllByTestId('chessboard-field')).toHaveLength(8);
    });
});
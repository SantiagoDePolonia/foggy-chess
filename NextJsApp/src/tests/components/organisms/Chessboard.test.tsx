import React from 'react';
import { render } from '@testing-library/react';
import Chessboard from '../../../components/organisms/Chessboard';

describe('<Chessboard />', () => {
    it('renders', () => {
        const {getByTestId} = render(<Chessboard />);
        expect(getByTestId('chessboard')).toBeTruthy();
    });

    it('displays 64 fields', () => {
        const {getAllByTestId} = render(<Chessboard />);
        expect(getAllByTestId('chessboard-field')).toHaveLength(64);
    });
});

import React from 'react';
import GamePage from '../../../pages/g/[id]';
import { render } from '@testing-library/react';

describe('<GamePage />', () => {
    it('renders', () => {
        const { getByTestId } = render(<GamePage />);
        expect(getByTestId('game-page')).toBeTruthy();
    });
});

import { createMuiTheme } from '@material-ui/core/styles';
import { lime, brown, red } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: lime[600],
    },
    secondary: {
      main: brown[600],
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
import React, {useContext} from 'react';
import store from '../classes/Store';
import {observer} from 'mobx-react';

export const GlobalContext = React.createContext(store);

export const Provider = ({children, ...props}) => (
    <GlobalContext.Provider value={store} {...props}>
        {children}
    </GlobalContext.Provider>
);

export default function connect(Component: React.ComponentType<any | string>, storeToProps = null) {
    const ObservedComponent = observer(Component);
    return observer(function ComponentWrapper(props) {
        const context = useContext(GlobalContext);
        const store = context.chessboardStore;

        if(storeToProps) {
            const mappedStore = storeToProps(store, props);
            return <ObservedComponent {...props} {...mappedStore} />;
        }

        return <ObservedComponent {...props} store={store} />;
    });
}

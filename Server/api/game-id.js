const initGame = require('foggy-chess-data/build/queries/initGame.js').default;

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction


async function gameId(req, res) {
  const {playerId} = req.body;
  const gameId = await initGame(playerId);
  res.statusCode = 200;
  res.json({
      id: gameId
  });
};

exports.default = gameId;

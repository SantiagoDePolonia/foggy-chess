const path = require('path');
const process = require('process');

require('dotenv').config({
    path: path.resolve(process.cwd(), '.env.local')
});

const app = require('express')();

const cors = require('cors');

app.use(cors());

const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const http = require('http').Server(app);

const io = require('socket.io')(http, {
    cors: {
      origin: "http://localhost:3000",
      methods: ["GET", "POST"]
    }
});

const apiGameId = require('./api/game-id.js').default;

const getGameIdFromSocket = require('./helpers/getGameIdFromSocket.js').default;
const getPlayerIdFromSocket = require('./helpers/getPlayerIdFromSocket.js').default;

const getChessboardByGameId = require('foggy-chess-data/build/queries/getChessboardByGameId.js').default;
const setChessboardStateByGameId = require('foggy-chess-data/build/queries/setChessboardStateByGameId.js').default;

const setPlayerRoleInGame = require('foggy-chess-data/build/queries/setPlayerRoleInGame.js').default;
const ChessboardArbitrator = require('foggy-chess-logic/build/classes/ChessboardArbitrator.js').default;
const Position = require('foggy-chess-logic/build/classes/Position').default;

app.post('/api/game-id', jsonParser, apiGameId);

// this can be used for testing WebSocket and socket.io library
// app.get('/', function(req, res) {
//    res.sendFile(__dirname + '/index.html');
// });

function getOppositeRole(role) {
  return role === "white" ? "black" : "white";
}

function getGameRoomName(gameId, role) {
  return 'game ' + gameId + ' ' + role;
}

function emitGameState(socket, chessboardArbitrator, role) {
  const gameId = getGameIdFromSocket(socket);
  const opponentRole = getOppositeRole(role);
  const playerResponse = chessboardArbitrator.getChessboard(role);
  const opponentResponse  = chessboardArbitrator.getChessboard(opponentRole);

  socket.emit('gameState', playerResponse);
  socket.to(getGameRoomName(gameId, opponentRole)).emit('gameState', opponentResponse);
}

async function getChessboardArbiterInstance(gameId, games) {
  if(!games[gameId]) {
    const chessboard = await getChessboardByGameId(gameId);
    const chessboardArbitrator = new ChessboardArbitrator(chessboard.chessboardState);
    games[gameId] = chessboardArbitrator;
  }
  return games[gameId];
}

const games = {};

// Whenever someone connects this gets executed
io.on('connection', async function(socket) {

  const gameId = getGameIdFromSocket(socket);
  const playerId = getPlayerIdFromSocket(socket);
  const role = await setPlayerRoleInGame(gameId, playerId);
  console.log('A player connected', gameId, playerId, role);

  if(role !== "other") {
    const chessboardArbitrator = await getChessboardArbiterInstance(gameId, games);

    socket.join(getGameRoomName(gameId, role));
  
    emitGameState(socket, chessboardArbitrator, role);
  
    socket.on('move', async (from, to) => {
      console.log("move", from, to);

      const pFrom = new Position().setFromName(from);
      const pTo = new Position().setFromName(to);
      
      if(chessboardArbitrator.movePiece(pFrom, pTo)) {
        await setChessboardStateByGameId(gameId, {
          rows: chessboardArbitrator.chessboardData.rows,
          turn: chessboardArbitrator.chessboardData.turn
        });
        emitGameState(socket, chessboardArbitrator, role);
      }
  
    });    
  }

  // Whenever someone disconnects this piece of code executed
  socket.on('disconnect', function () {
    console.log('A player disconnected', gameId, playerId, role);
  });
});

http.listen(4001, function() {
   console.log('listening on *:4001');
});

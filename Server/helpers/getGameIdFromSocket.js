function getGameIdFromSocket(socket) {
    return socket.handshake.query.gameId;
}

exports.default = getGameIdFromSocket;
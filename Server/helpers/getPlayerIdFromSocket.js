function getPlayerIdFromSocket(socket) {
    return socket.handshake.query.playerId;
}

exports.default = getPlayerIdFromSocket;
